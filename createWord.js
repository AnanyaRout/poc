import { Document, HeadingLevel, Media, Packer, Paragraph, TextRun, TableCell } from "docx";
import * as fs from "fs";

const createWord = (params, callBack) => {
    const doc = new Document();
    const rows = params.room.map((room) => {
        return new Paragraph({
            children: [
                new TextRun({
                    text: room.name + " :  ",
                    bold: true,
                }),
                new TextRun({
                    text: room.description,
                }),
            ],
        })
    });
    doc.addSection({
        children: [
            new Paragraph({
                text: "ROOM DETAILS",
                heading: HeadingLevel.HEADING_1,
            }),
        ],
    });
    doc.addSection({
        children: rows,
    });
    Packer.toBuffer(doc).then((buffer) => {
        fs.writeFileSync("MyDocuments.docx", buffer);
    });
    callBack();
}
export {
    createWord
}